import { Component, OnInit, Input } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home-search',
  templateUrl: './home-search.component.html',
  styleUrls: ['./home-search.component.css']
})
export class HomeSearchComponent implements OnInit {

                             
 
  currentItem = 'Television';

  data : any= [];
  searchForm  = this.fb.group({
    search:[''],
  });

  search : any;
  constructor(private _requestService : RequestService, private fb: FormBuilder,) { }


  ngOnInit(): void {
    console.log('parent')
  }
  onSubmit(): void {
    this._requestService. getRequest(this.searchForm.value.search).subscribe((res) : any=>  this.data = res);
  }
 
}
