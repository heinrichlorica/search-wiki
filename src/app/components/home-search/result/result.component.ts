import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  @Input() items = '';

  name : any =[];
  link : any =[];
  constructor() { }

  ngOnChanges(){
    this.name =this.items[1]
    this.link =this.items[3]
    console.log(this.items)
  }
  ngOnInit(): void {
    console.log('child')
  }

}
