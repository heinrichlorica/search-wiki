import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// import 'rxjs/Rx';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  
  // public action : string;
  public id : any;
  private url = environment.apiBaseUrl;
  constructor(private http: HttpClient,) { }

   // METHOD : GET
	// DETAILS : Get All Records
  getRequest(search: any){

    let url = `${this.url}?origin=*&action=opensearch&search=${search}`;
    return this.http.get(url)
      .map((res: any): any => this.extractData(res));
  }

  

    // METHOD : GET
    // DETAILS : Get Record Details by ID
    getRequestById(id: any)
    {
      let url = this.url + id;
      return this.http.get(url).map((res: any) => this.extractData(res));
    }

    private extractData(res: any) {

      let body = null;
  
      if (res != null) {
          if (res["_body"] != "" && res["_body"]) {
              body = res.json();
          }
          else	{
            body = res;
          }
      }
          return body;
    }
}
